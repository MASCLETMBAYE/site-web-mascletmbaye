<?php
session_start();
 
if (empty($_SESSION['pseudo'])) {
    header('Location: pageConnection.php');
    exit;
}
 
$redirects = array(
    'admin'  => 'accueilAdmin.php',
    'visiteur'   => 'accueilVisiteur.php',
    'rspbateau' => 'accueilResp.php',
);
 
$role = $_SESSION['pseudo']['role'];
header("Location: {$redirects[$role]}");
exit;
?>