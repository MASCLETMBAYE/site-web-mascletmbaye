<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <img src="img/logo.png" alt="logo" width="100"/>
        <link rel="stylesheet" href="css/styles.css" />
        <title>Page Responsable de bâteau</title>
        <style type="text/css">
        </style>
    </head>
    <body>
    <body style="background-image: url('img/fondd.jpg');">
        
               <?php
                include 'navResp.php';
               ?>
    
        </div>
        <div id="content">
            <div class="row">
                        <h1><img src="img/ico_epingle.png" alt="Catégorie voyage" class="ico_categorie" />Bienvenue sur le site ARMADA</h1>
                        <h2>Vous êtes connecté en tant que responsable de bâteau</h2>
                        <p>L'Armada est un large rassemblement de grands voiliers organisé à Rouen, dans la Seine-Maritime.</p>
                        <p>Il est un des évènements importants du monde de la mer.</p> 
                        <p>Il a lieu tous les quatre à six ans sur les quais de la Seine, au sein même de la métropole normande.</p>
                        <p>Cette manifestation dure en général une dizaine de jours.</p>
            </div>
            <div class="thumbnail">
                        <h1>Video Armada</h1>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/8M8VcrOMRhc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
       
        

    </body>

    <footer>
        <p> &copy;2018 Mbaye/Masclet Copyright All Rights Reserved. </p>
        <p><img src="img/facebook.png" alt="Facebook" />&emsp;<img src="img/twitter.png" alt="Twitter" />&emsp;<img src="img/vimeo.png" alt="Vimeo" />&emsp;<img src="img/flickr.png" alt="Flickr" />&emsp;<img src="img/rss.png" alt="RSS" /></p>

    </footer>
</html>
