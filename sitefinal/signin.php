<?php
    if(isset($_POST['lesubmit']))
    {
        extract($_POST);
        if(!empty($lepseudo) && !empty($lemdp) && !empty($email) && !empty($prenom) && !empty($nom) && !empty($dateN) && !empty($sexe) )
        {
            $q = $db -> prepare("INSERT INTO inscrit(nom,prenom,dateN,sexe,email,pseudo,mdp,rolee) VALUES (:nom , :prenom , :dateN , :sexe , :email , :pseudo , :mdp, :rolee)  ");
            $q -> execute([
                'nom' => $nom,
                'prenom' => $prenom,
                'dateN' => $dateN,
                'sexe' => $sexe,
                'email' => $email,
                'pseudo' => $lepseudo,
                'mdp' => $lemdp,
                'rolee' => $lerole
                
            ]);
           
            {
                   echo "Vous êtes bien inscrits !";
            }
          
        }
        else
        {
            echo "Veuillez compléter l'ensemble des champs !";
        }
    }
?>