<?php


    if(isset($_POST['formlogin']))
    {
        extract($_POST);
        
        if(!empty($lpseudo) && !empty($lmdp) && !empty($roleee))
        {
            $q = $db -> prepare ("SELECT * FROM inscrit WHERE pseudo = ?");
            $q -> execute(array($_POST['lpseudo']));
                            
            $result = $q -> fetch();
           
            if(!empty($result))
            {
                if($result['mdp']==$_POST['lmdp'])
                {
                    if($result['rolee'] == "Visiteur")
                    {
                    header('Location : accueilVisiteur.php');
                    }
                    if($result['rolee'] == "Admin")
                    {
                        header('Location : accueilAdmin.php');
                    }
                    if($result['rolee'] == "R.Bateau")
                    {
                        header('Location : accueilResp.php');
                    }
                 }
                 else
                 {
                     echo "Le Compte n'existe pas";
                 }
            }

            else
            {
                echo "Le Compte n'existe pas";
            }
        }
        else
        {
            echo "Veuillez compléter l'ensemble des champs";
        }
    }
    
?>